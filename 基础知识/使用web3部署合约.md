参考链接:

- [web3.js编译Solidity，发布，调用全部流程（手把手教程）](http://web3.tryblockchain.org/web3-js-in-action.html)
- [以太坊(Ethereum)私链建立 、合约编译、部署完全教程(1)](http://liyuechun.org/2017/10/14/eth-private-blockchain/)


由于所有其它工具都或多或少的是对web3.js的底层函数的包装，所以对web3.js使用流程有个认识之后，也能更好的入门，使用相关的工具。


## 准备工作

### 安装web3

安装版本: [0.20.6](https://github.com/ethereum/wiki/wiki/JavaScript-API)

在写这文档为止， 最新的版本为1.0.0, 但是 [0v1.0.0-beta.34](http://web3js.readthedocs.io/en/1.0/index.html)， 但是为测试版， 不建议使用。

```shell
yarn add web3@0.20.6
```


### 以太坊的节点

由于整个合约代码的执行需要一个虚拟机环境，所以在开始之前，我们不得不安装一个实现了以太坊虚拟机的节点。

可以选择一个轻量级的节点，比如ganache-cli（之前叫EtherumJS TestRPC），它是一个完整的在内存中的区块链仅仅存在于你开发的设备上。**它在执行交易时是实时返回，而不等待默认的出块时间**，这样你可以快速验证你新写的代码，当出现错误时，也能即时反馈给你。

安装:

```shell
npm install -g ganache-cli
```

运行:

```shell
ganache-cli
```

```shell
Ganache CLI v6.1.0 (ganache-core: 2.1.0)

Available Accounts
==================
(0) 0x2e803c415467457b043ac0acdfb4d02970fc2237
...

Private Keys
==================
(0) 08fc54463292e1b0afdbfebcbea49b84b7162dd8a7e16918ba24cbe7b6d5270b
...

HD Wallet
==================
Mnemonic:      pig year clown live erode milk neck column scene mystery fire narrow
Base HD Path:  m/44'/60'/0'/0/{account_index}

Listening on localhost:8545
```


## 合约编译

### 一个简单的合约

```
pragma solidity ^0.4.0;

contract Calc{
  /*区块链存储*/
  uint count;

  /*执行会写入数据，所以需要`transaction`的方式执行。*/
  function add(uint a, uint b) returns(uint){
    count++;
    return a + b;
  }

  /*执行不会写入数据，所以允许`call`的方式执行。*/
  function getCount() constant returns (uint){
    return count;
  }
}
```

### 编译合约

代码拷贝到https://remix.ethereum.org，编译，然后拷贝字节码和ABI（点击detail显示）.

![image.png](https://upload-images.jianshu.io/upload_images/1914208-db3e1940f0a74751.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


新建demo01.js文件:

```js
const abiDefinition = JSON.parse('[{\"constant\":false,\"inputs\":[{\"name\":\"a\",\"type\":\"uint256\"},{\"name\":\"b\",\"type\":\"uint256\"}],\"name\":\"add\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\",\"stateMutability\":\"nonpayable\"},{\"constant\":true,\"inputs\":[],\"name\":\"getCount\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\",\"stateMutability\":\"view\"}]');

const deployCode = '0x606060405260d58060106000396000f360606040526000357c010000000000000000000000000000000000000000000000000000000090048063771602f7146043578063a87d942c14607a57603f565b6002565b3460025760646004808035906020019091908035906020019091905050609f565b6040518082815260200191505060405180910390f35b346002576089600480505060c4565b6040518082815260200191505060405180910390f35b60006000600081815054809291906001019190505550818301905060be565b92915050565b6000600060005054905060d2565b9056';
let calcContract = web3.eth.contract(abiDefinition);
```


### 发布合约

```js
let myContractReturned = calcContract.new({
    data: deployCode,
    from: deployeAddr,
    gas: 100000, 
}, function(err, myContract) {
    if (!err) {
        // 注意：这个回调会触发两次
        //一次是合约的交易哈希属性完成
        //另一次是在某个地址上完成部署

        // 通过判断是否有地址，来确认是第一次调用，还是第二次调用。
        if (!myContract.address) {
            console.log("contract deploy transaction hash: " + myContract.transactionHash) //部署合约的交易哈希值

            // 合约发布成功
        } else {
        }
});
```

注意, gas必须指定， 不然会宝out of gas错误.

可以设置的大点， 多了会退回来， 预估手续费：

```js
web3.eth.estimateGas({data: deployCode})
```

你需要写入的数据越大， 手续费就越多.


#### 调用合约

> 由于web3.js封装了合约调用的方法。我们可以使用可以使用web3.eth.contract的里的sendTransaction来修改区块链数据。在这里有个坑，有可能会出现Error: invalid address，原因是没有传from，交易发起者的地址。在使用web3.js的API都需留意，出现这种找不到地址的，都看看from字段吧。

```js
//使用transaction方式调用，写入到区块链上
myContract.add.sendTransaction(1, 2,{
    from: deployeAddr
});

console.log("after contract deploy, call:" + myContract.getCount.call());
```

#### 使用web3.js编译，发布，调用的完整源码

```js
let Web3 = require('web3');

if (typeof web3 !== 'undefined') {
  web3 = new Web3(web3.currentProvider);
} else {
  // set the provider you want from Web3.providers
  web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8545"));
}

var accounts = web3.eth.accounts;
var deployeAddr = accounts[0];

console.log(deployeAddr);
console.log(web3.eth.getBalance(deployeAddr));

const abiDefinition = JSON.parse('[{\"constant\":false,\"inputs\":[{\"name\":\"a\",\"type\":\"uint256\"},{\"name\":\"b\",\"type\":\"uint256\"}],\"name\":\"add\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\",\"stateMutability\":\"nonpayable\"},{\"constant\":true,\"inputs\":[],\"name\":\"getCount\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"type\":\"function\",\"stateMutability\":\"view\"}]');
let calcContract = web3.eth.contract(abiDefinition);
const deployCode = '0x606060405260d58060106000396000f360606040526000357c010000000000000000000000000000000000000000000000000000000090048063771602f7146043578063a87d942c14607a57603f565b6002565b3460025760646004808035906020019091908035906020019091905050609f565b6040518082815260200191505060405180910390f35b346002576089600480505060c4565b6040518082815260200191505060405180910390f35b60006000600081815054809291906001019190505550818301905060be565b92915050565b6000600060005054905060d2565b9056';


let myContractReturned = calcContract.new({
  data: deployCode,
  from: deployeAddr,
  gas: 1000000,
}, function (err, myContract) {
  if (!err) {
    // 注意：这个回调会触发两次
    //一次是合约的交易哈希属性完成
    //另一次是在某个地址上完成部署

    // 通过判断是否有地址，来确认是第一次调用，还是第二次调用。
    if (!myContract.address) {
      console.log("contract deploy transaction hash: " + myContract.transactionHash) //部署合约的交易哈希值

      // 合约发布成功后，才能调用后续的方法
    } else {
      console.log("contract deploy address: " + myContract.address) // 合约的部署地址

      //使用transaction方式调用，写入到区块链上
      myContract.add.sendTransaction(1, 2, {
        from: deployeAddr
      });

      console.log("after contract deploy, call:" + myContract.getCount.call());
    }

    // 函数返回对象`myContractReturned`和回调函数对象`myContract`是 "myContractReturned" === "myContract",
    // 所以最终`myContractReturned`这个对象里面的合约地址属性也会被设置。
    // `myContractReturned`一开始返回的结果是没有设置的。
  }
  console.log(err);
});

console.log("returned deployed didn't have address now: " + myContractReturned.address);
```


然后使用运行, 查看结果:

```shell
node demo01.js
```

